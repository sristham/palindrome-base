package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromRegular() {
		boolean result = Palindrome.isPalindrome("racecar");
		assertTrue("This is not a palindrome", result);
	}

	@Test
	public void testIsPalindromeException() {
		boolean result = Palindrome.isPalindrome("thisfails");
		assertFalse("Test not created.", result);
	}

	@Test
	public void testIsPalindromeBoundaryIn() {
		boolean result = Palindrome.isPalindrome("Taco cat");
		assertTrue("This is not a palindrome", result);
	}

	@Test
	public void testIsPalindromeBoundaryOut() {
		boolean result = Palindrome.isPalindrome("race on car");
		assertFalse("This is not a palindrome", result);
	}

}
